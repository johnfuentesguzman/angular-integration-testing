import { routes } from './app.routes';
import { UsersComponent } from '../app/users/users.component';

describe('routes', () => {
    it('should contain user path', () => {
        expect(routes).toContain({ path: 'users', component: UsersComponent })
    });
});