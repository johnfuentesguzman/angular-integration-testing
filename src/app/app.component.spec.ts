/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'; 
import { By } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterOutlet, RouterLinkWithHref } from '@angular/router';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])], // mocking app.module.ts (RouterModule.forRoot(routes),) using a default angular mock fot it..
      declarations: [ AppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges(); 
  });

  it('shpuld have router outlet', ()=> {
    let htmlTag = fixture.debugElement.query(By.directive(RouterOutlet)); // RouterOutlet has been mocked using imports: [RouterTestingModule.withRoutes([])] (this .spec file)
    expect(htmlTag).not.toBeNull();
  })

  it('shpuld have todos link ', ()=> {
    // queryall get all the HTML elements like an array
    let htmlTag = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref)); // RouterOutlet has been mocked using imports: [RouterTestingModule.withRoutes([])] (this .spec file)
    expect(htmlTag).not.toBeNull();
    // routerLink=" routerLink="todos">">
    let index = htmlTag.findIndex(de => {
      console.log(de) 
      de.attributes['routerLink'] === 'todos'});
    expect(index).toEqual(-1);
  })
});
