
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UserDetailsComponent } from './user-details.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, empty, Subject } from 'rxjs';

  class RouterStub {
    navigate(params){

    }
  };

  class ActivatedRouteStub {
    private subject = new Subject();
    
    push(value){ // this method pushes a fake value into the observable
      this.subject.next(value)
    }

    get params(){ // public function for mocking .params method for  ActivatedRoute (see component constructor) in the component (it returns a observable)
      return this.subject.asObservable();
    }

    //params: Observable<any> = empty();
  }

  describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailsComponent ],
      providers: [
        {
          provide: Router, useClass: RouterStub, // mocking a fake router
        },
        {
          provide: ActivatedRoute, useClass: ActivatedRouteStub // mocking a fake ActivatedRoute used in the component
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect to user page', () => {
    let router =  TestBed.get(Router); //  calling fake router created im this test file
    let spy = spyOn(router, 'navigate') // getting router dependencie method

    component.save();
    expect(spy).toHaveBeenCalledWith(['users']);
    // expect(component).toBeTruthy();
  });

  it('should return 404 page in case the user dosent exit', () =>{
    let router =  TestBed.get(Router); 
    let spy = spyOn(router, 'navigate') // getting router dependencie method

    let route: ActivatedRouteStub =  TestBed.get(ActivatedRoute);
    route.push({id: 0 }); // pushing a value into the component (see ngOnInit). with this value user-details.component.ts will be capable to proceed with its suscripcion
    expect(spy).toHaveBeenCalledWith(['not-found']);

  });
});
