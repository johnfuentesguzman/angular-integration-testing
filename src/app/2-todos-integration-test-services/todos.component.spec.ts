import { TodoService } from '../2-todos-integration-test-services/todo.service';
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { TodosComponent } from './todos.component';
import { from } from 'rxjs';


// When you get to Lecture 6 (Providing Dependencies), be sure
// to remove "x" from "xdescribe" below. like that the test will not be read by karma : xdescribe('TodosComponent', () => {  });


// ************************************* INTEGRATION TEST EXAMMPLE: testing injectable service, .Ts behaviour and dependencies 
xdescribe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule], // Providing dependecies (see  app.module.ts)
      declarations: [ TodosComponent ],
      providers: [TodoService] // provided the TodoService as a dependency to TodosComponent. 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
  });

  it('should load todos service by ngOnInit()', () => { //  ngOnInit
    // fixture.debugElement.injector.get(TodoService); // used just in case you want to instance a dependencies or injectables called in just the specific component to test
    let service =  TestBed.get(TodoService); // TESTBED is used to get dependencies and injectables (services), from the app.module (providers)
    
    spyOn(service, 'getTodos').and.returnValue(from([ 1,2,3])) // spyon to access to services or dependicies methods and mock its response, from to return a observable from an array
    fixture.detectChanges();
    component.ngOnInit();
    expect(component.todos.length).toBe(3);
  });
});
