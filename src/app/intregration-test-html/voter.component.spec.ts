import { TestBed, ComponentFixture } from '@angular/core/testing';
import { VoterComponent } from './voter.component';
import { By } from '@angular/platform-browser';
describe('VoterComponent', () => {
  let component: VoterComponent;
  let fixture: ComponentFixture<VoterComponent>; /// yp have access to TS and HTML template of this component tested

  beforeEach(() => {
    TestBed.configureTestingModule({ // TESTBED is used to get dependencies and injectables (services)
      declarations: [VoterComponent] // instacing the component to test
    });
    fixture = TestBed.createComponent(VoterComponent);
    component = fixture.componentInstance;
  });

  it('should render into UI render total Votes', () => { // validating interpolation binding
    component.othersVote = 20;
    component.myVote = 1;

    fixture.detectChanges(); // to hanle any change  in code and DOM of this component tested

    let debugElement = fixture.debugElement.query(By.css('.vote-count')) // go trough DOM and get the tag id

    let el: HTMLElement=  debugElement.nativeElement; // test html element (ccs does not a)
    expect(el.innerText).toContain('21');

    });

    it('should use highlighted class in case of my vote is 1', () => { // validating interpolation binding
      component.myVote = 1;
  
      fixture.detectChanges(); // to hanle any change  in code and DOM of this component tested
  
      let debugElement = fixture.debugElement.query(By.css('.glyphicon-menu-up')) // go trough DOM and get the tag id
  
      //let el: HTMLElement=  debugElement.nativeElement; // we are not getting htm
      expect(debugElement.classes['highlighted']).toBeTruthy();
      });


      it('should use highlighted class (Getting BY ID) class in case of my vote is 1', () => { // validating interpolation binding
        component.myVote = 1;
    
        fixture.detectChanges(); // to hanle any change  in code and DOM of this component tested
    
        let debugElement = fixture.debugElement.query(By.css('#menuUp'))  // go trough DOM and get the tag id
        expect(debugElement.classes['highlighted']).toBeTruthy();

      });


      // Integration test below
      it('should increment total votes but the user hits upvote html button', () => { // validating interpolation binding
        component.myVote = 0;
    
        fixture.detectChanges(); // to hanle any change  in code and DOM of this component tested
    
        let button = fixture.debugElement.query(By.css('#menuUp'))  // go trough DOM and get the tag id
        button.triggerEventHandler('click', null); // listening click event
        expect(component.totalVotes).toBe(1)

      });


});
