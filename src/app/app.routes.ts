import { HomeComponent } from './home/home.component';
import { TodosComponent } from '../app/2-todos-integration-test-services/todos.component'
import { UsersComponent } from './users/users.component'; 
import { UserDetailsComponent } from '../app/navigate-router/user-details.component';

export const routes = [
  { path: 'users/:id', component: UserDetailsComponent },
  { path: 'users', component: UsersComponent },
  { path: 'todos', component: TodosComponent },
  { path: '', component: HomeComponent },
];