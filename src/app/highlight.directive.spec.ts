/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HighlightDirective } from './highlight.directive';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core'; 

@Component({
  template: `
    <p id="redColor" highlight="red">First</p>
    <p id="default" highlight>Second</p>
  `
})
class DirectiveHostComponent { 
}

describe('HighlightDirective', () => {
  let fixture: ComponentFixture<DirectiveHostComponent>;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectiveHostComponent, HighlightDirective ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectiveHostComponent);
    fixture.detectChanges(); 
  });

  it('should hightlight the first element with red color', () => {
    let element = fixture.debugElement.queryAll(By.css('p#redColor'))[0];

    console.log('directive test element... ', element);
    expect(element.nativeElement.style.backgroundColor).toBe('red');

  });

  it('should hightlight the first element with default directive color', () => {
    let element = fixture.debugElement.queryAll(By.css('p'))[1];
    let directive = element.injector.get(HighlightDirective); // adding component diretive to this tesing component templates
    expect(element.nativeElement.style.backgroundColor).toBe(directive.defaultColor);

  });
});
